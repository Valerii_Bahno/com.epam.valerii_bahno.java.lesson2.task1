public class ArrayProcessing {
    public static void main(String[] args) {
        ArrayProcessing app = new ArrayProcessing();
        app.arrayProcess();
    }

    /*Option 2 - In an array of integers, determine the sum of elements in even positions*/
    public void arrayProcess() {
        /*Condition - the array contains only integers from -10 to 10, the number of elements in the array is 20*/
        int[] mas = new int[] {1, 3, 5, 6, 8, 3, -5, -2, 9, 1, 4, -8, 2, 0, -7, 3, -9, 4, 2, 7};
        int sum = 0;
        System.out.println("Source array: ");
        for (int i = 0; i < mas.length; i++) {
            if (i%2 == 0) {
                sum = sum + mas[i];
            }
            System.out.print(mas[i] + " ");
        }
        System.out.println();
        System.out.println("The sum of elements in even positions: " + sum);
    }
}
